# Synq.ErrorObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | 
**name** | **String** |  | 
**message** | **String** |  | 
**details** | **Object** |  | [optional] 


