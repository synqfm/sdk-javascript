# Synq.UploadParameterObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aWSAccessKeyId** | **String** |  | [optional] 
**contentType** | **String** |  | [optional] 
**policy** | **String** |  | [optional] 
**signature** | **String** |  | [optional] 
**acl** | **String** |  | [optional] 
**key** | **String** |  | [optional] 
**action** | **String** |  | [optional] 


