# Synq.StreamConfigurationObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rtmpHost** | **String** |  | [optional] 
**rtmpPort** | **String** |  | [optional] 
**rtmpKey** | **String** |  | [optional] 
**rtmpUrl** | **String** |  | [optional] 


