(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', '../model/StreamConfigurationObject', '../model/ErrorObject'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/StreamConfigurationObject'), require('../model/ErrorObject'));
  } else {
    // Browser globals (root is window)
    if (!root.Synq) {
      root.Synq = {};
    }
    root.Synq.StreamApi = factory(root.Synq.ApiClient, root.Synq.StreamConfigurationObject, root.Synq.ErrorObject);
  }
}(this, function(ApiClient, StreamConfigurationObject, ErrorObject) {
  'use strict';

  /**
   * Stream service.
   * @module api/StreamApi
   * @version 1.7.0
   */

  /**
   * Constructs a new StreamApi. 
   * @alias module:api/StreamApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the start operation.
     * @callback module:api/StreamApi~startCallback
     * @param {String} error Error message, if any.
     * @param {module:model/StreamConfigurationObject} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Return parameters needed for transmitting a video stream to our RTMP receiver.
     * Return parameters needed for transmitting a video stream to our RTMP receiver.\n\n#### *Example request*:\n\n```shell\ncurl -s https://api.synq.fm/v1/stream/start \\\n    -F api_key=${SYNQ_API_KEY} \\\n    -F video_id=2b795c9e64648e08100ce6df6cf4b989\n```\n\n#### *Example response*:\n\n```json\n{\n    \&quot;rtmp_host\&quot;: \&quot;ec2-54-171-205-23.eu-west-1.compute.amazonaws.com\&quot;,\n    \&quot;rtmp_port\&quot;: \&quot;1935\&quot;,\n    \&quot;rtmp_key\&quot;: \&quot;live/ce29d1cb5da48a564d2c9f62be7ade06\&quot;,\n    \&quot;rtmp_url\&quot;: \&quot;rtmp://ec2-54-171-205-23.eu-west-1.compute.amazonaws.com:1935/live/ce29d1cb5da48a564d2c9f62be7ade06\&quot;\n}\n```\n\nTo transmit the video stream, enter the parameters into your streaming software.\nFor example, for FFmpeg:\n\n```shell\n    ffmpeg -re -i my_video_file.mp4 -f flv rtmp://ec2-54-171-205-23.eu-west-1.compute.amazonaws.com:1935/live/ce29d1cb5da48a564d2c9f62be7ade06\n```\n\nAfter two hours of streaming or fifteen minutes after the last video packet was received,\nwhichever happends first, the stream is considered done.\nAt that point, the recording will be processed for later playback,\nand you will be unable to continue streaming this particular video.\n
     * @param {String} apiKey 
     * @param {String} videoId The ID of the video you are going to stream into.
     * @param {module:api/StreamApi~startCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/StreamConfigurationObject}
     */
    this.start = function(apiKey, videoId, callback) {
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling start";
      }

      // verify the required parameter 'videoId' is set
      if (videoId == undefined || videoId == null) {
        throw "Missing the required parameter 'videoId' when calling start";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'video_id': videoId
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = StreamConfigurationObject;

      return this.apiClient.callApi(
        '/stream/start', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
