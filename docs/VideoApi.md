# Synq.VideoApi

All URIs are relative to *http://api.staging.synq.fm/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](VideoApi.md#create) | **POST** /video/create | Create a new video, optionally setting some metadata fields.
[**details**](VideoApi.md#details) | **POST** /video/details | Return details about a video.
[**query**](VideoApi.md#query) | **POST** /video/query | Perform a JavaScript query to return video objects matching any desired criteria.
[**update**](VideoApi.md#update) | **POST** /video/update | Update a video&#39;s metadata.
[**upload**](VideoApi.md#upload) | **POST** /video/upload | Return parameters needed for uploading a video file.
[**uploader**](VideoApi.md#uploader) | **POST** /video/uploader | Return embeddable url to an uploader widget


<a name="create"></a>
# **create**
> Object create(apiKey, opts)

Create a new video, optionally setting some metadata fields.

Create a new video, optionally setting some metadata fields. You may optionally set some of the metadata associated with the video. Only fields inside the \&quot;userdata\&quot; field can be set.

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.VideoApi()

var apiKey = "apiKey_example"; // {String} 

var opts = { 
  'userdata': "userdata_example" // {String} Additional metadata that will be associated with the video
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.create(apiKey, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **userdata** | **String**| Additional metadata that will be associated with the video | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="details"></a>
# **details**
> Object details(apiKey, videoId)

Return details about a video.

Return details about a video. You may optionally request that only some of the metadata fields are returned.

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.VideoApi()

var apiKey = "apiKey_example"; // {String} 

var videoId = "videoId_example"; // {String} ID of the video to retrieve the metadata from


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.details(apiKey, videoId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **videoId** | **String**| ID of the video to retrieve the metadata from | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="query"></a>
# **query**
> [Object] query(apiKey, filter)

Perform a JavaScript query to return video objects matching any desired criteria.

Find videos matching any criteria, by running a JavaScript function over each video object. A detailed tutorial on how to use this functionality is available on the documentation page.

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.VideoApi()

var apiKey = "apiKey_example"; // {String} 

var filter = "filter_example"; // {String} JavaScript code to be run over each video object, to determine what should be returend.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.query(apiKey, filter, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **filter** | **String**| JavaScript code to be run over each video object, to determine what should be returend. | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="update"></a>
# **update**
> Object update(apiKey, videoId, source)

Update a video&#39;s metadata.

Update a video&#39;s metadata through JavaScript code. Only fields inside the \&quot;userdata\&quot; object can be set.

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.VideoApi()

var apiKey = "apiKey_example"; // {String} 

var videoId = "videoId_example"; // {String} The ID of the video whose metadata will be updated

var source = "source_example"; // {String} JavaScript code to execute on the video object.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.update(apiKey, videoId, source, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **videoId** | **String**| The ID of the video whose metadata will be updated | 
 **source** | **String**| JavaScript code to execute on the video object. | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="upload"></a>
# **upload**
> UploadParameterObject upload(apiKey, videoId)

Return parameters needed for uploading a video file.

Return parameters needed for uploading a video file to Amazon Simple Storage Service. See http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-post-example.html as well as the language-specific code-examples.\n#### *Example request*\n```shell\ncurl -s https://api.synq.fm/v1/video/upload \\\n  -F api_key=${SYNQ_API_KEY} \\\n  -F video_id=2d81c30ce62f4dfdb501dbca96c7ae56\n```\n\n#### *Example response*\n```json\n{\n  \&quot;action\&quot;: \&quot;https://synqfm.s3.amazonaws.com/\&quot;,\n  \&quot;AWSAccessKeyId\&quot;: \&quot;AKIAIP77Y7MMX3ITZMFA\&quot;,\n  \&quot;Content-Type\&quot;: \&quot;video/mp4\&quot;,\n  \&quot;Policy\&quot;: \&quot;eyJleHBpcmF0aW9uIiA6ICIyMDE2LTA0LTIyVDE5OjAyOjI2LjE3MloiLCAiY29uZGl0aW9ucyIgOiBbeyJidWNrZXQiIDogInN5bnFmbSJ9LCB7ImFjbCIgOiAicHVibGljLXJlYWQifSwgWyJzdGFydHMtd2l0aCIsICIka2V5IiwgInByb2plY3RzLzZlLzYzLzZlNjNiNzUyYTE4NTRkZGU4ODViNWNjNDcyZWRmNTY5L3VwbG9hZHMvdmlkZW9zLzJkLzgxLzJkODFjMzBjZTYyZjRkZmRiNTAxZGJjYTk2YzdhZTU2Lm1wNCJdLCBbInN0YXJ0cy13aXRoIiwgIiRDb250ZW50LVR5cGUiLCAidmlkZW8vbXA0Il0sIFsiY29udGVudC1sZW5ndGgtcmFuZ2UiLCAwLCAxMDk5NTExNjI3Nzc2XV19\&quot;,\n  \&quot;Signature\&quot;: \&quot;ysqDemlKXKr6hKzVFP0hCGgf/cs=\&quot;,\n  \&quot;acl\&quot;: \&quot;public-read\&quot;,\n  \&quot;key\&quot;: \&quot;projects/6e/63/6e63b752a1854dde885b5cc472edf569/uploads/videos/2d/81/2d81c30ce62f4dfdb501dbca96c7ae56.mp4\&quot;\n}\n```\n\nTo upload the file, you can then make a multipart POST request to the URL in `action`, and for all the other parameters returned, set them as form parameters.\n\nGiven the parameters above, you would upload a file `test.mp4` using cURL like this:\n\n```shell\ncurl -s https://synqfm.s3.amazonaws.com/ \\\n  -F AWSAccessKeyId=\&quot;AKIAIP77Y7MMX3ITZMFA\&quot; \\\n  -F Content-Type=\&quot;video/mp4\&quot; \\\n  -F Policy=\&quot;eyJleHBpcmF0aW9uIiA6ICIyMDE2LTA0LTIyVDE5OjAyOjI2LjE3MloiLCAiY29uZGl0aW9ucyIgOiBbeyJidWNrZXQiIDogInN5bnFmbSJ9LCB7ImFjbCIgOiAicHVibGljLXJlYWQifSwgWyJzdGFydHMtd2l0aCIsICIka2V5IiwgInByb2plY3RzLzZlLzYzLzZlNjNiNzUyYTE4NTRkZGU4ODViNWNjNDcyZWRmNTY5L3VwbG9hZHMvdmlkZW9zLzJkLzgxLzJkODFjMzBjZTYyZjRkZmRiNTAxZGJjYTk2YzdhZTU2Lm1wNCJdLCBbInN0YXJ0cy13aXRoIiwgIiRDb250ZW50LVR5cGUiLCAidmlkZW8vbXA0Il0sIFsiY29udGVudC1sZW5ndGgtcmFuZ2UiLCAwLCAxMDk5NTExNjI3Nzc2XV19\&quot; \\\n  -F Signature=\&quot;ysqDemlKXKr6hKzVFP0hCGgf/cs=\&quot; \\\n  -F acl=\&quot;public-read\&quot; \\\n  -F key=\&quot;projects/6e/63/6e63b752a1854dde885b5cc472edf569/uploads/videos/2d/81/2d81c30ce62f4dfdb501dbca96c7ae56.mp4\&quot; \\\n  -F file=\&quot;@my_video_file.mp4\&quot;\n```\n\n

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.VideoApi()

var apiKey = "apiKey_example"; // {String} 

var videoId = "videoId_example"; // {String} The ID of the video you are going to upload into. The video needs to have been previously created.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.upload(apiKey, videoId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **videoId** | **String**| The ID of the video you are going to upload into. The video needs to have been previously created. | 

### Return type

[**UploadParameterObject**](UploadParameterObject.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="uploader"></a>
# **uploader**
> InlineResponse200 uploader(apiKey, videoId, opts)

Return embeddable url to an uploader widget

Returns an embeddable url, that contains an uploader widget that allows you to easily upload any mp4. Great way to simplify the uploading process for end users.

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.VideoApi()

var apiKey = "apiKey_example"; // {String} 

var videoId = "videoId_example"; // {String} The ID of the video you are going to upload into. The video needs to have been previously created.

var opts = { 
  'timeout': "2 hours" // {String} How long the uploader widget works for. Anything from '30 minutes' to '2 days'.
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.uploader(apiKey, videoId, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **videoId** | **String**| The ID of the video you are going to upload into. The video needs to have been previously created. | 
 **timeout** | **String**| How long the uploader widget works for. Anything from &#39;30 minutes&#39; to &#39;2 days&#39;. | [optional] [default to 2 hours]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: text/plain

