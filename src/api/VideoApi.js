(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient', '../model/ErrorObject', '../model/UploadParameterObject', '../model/InlineResponse200'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorObject'), require('../model/UploadParameterObject'), require('../model/InlineResponse200'));
  } else {
    // Browser globals (root is window)
    if (!root.Synq) {
      root.Synq = {};
    }
    root.Synq.VideoApi = factory(root.Synq.ApiClient, root.Synq.ErrorObject, root.Synq.UploadParameterObject, root.Synq.InlineResponse200);
  }
}(this, function(ApiClient, ErrorObject, UploadParameterObject, InlineResponse200) {
  'use strict';

  /**
   * Video service.
   * @module api/VideoApi
   * @version 1.7.0
   */

  /**
   * Constructs a new VideoApi. 
   * @alias module:api/VideoApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use, default to {@link module:ApiClient#instance}
   * if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the create operation.
     * @callback module:api/VideoApi~createCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create a new video, optionally setting some metadata fields.
     * Create a new video, optionally setting some metadata fields. You may optionally set some of the metadata associated with the video. Only fields inside the \&quot;userdata\&quot; field can be set.
     * @param {String} apiKey 
     * @param {Object} opts Optional parameters
     * @param {String} opts.userdata Additional metadata that will be associated with the video
     * @param {module:api/VideoApi~createCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Object}
     */
    this.create = function(apiKey, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling create";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'userdata': opts['userdata']
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = Object;

      return this.apiClient.callApi(
        '/video/create', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the details operation.
     * @callback module:api/VideoApi~detailsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Return details about a video.
     * Return details about a video. You may optionally request that only some of the metadata fields are returned.
     * @param {String} apiKey 
     * @param {String} videoId ID of the video to retrieve the metadata from
     * @param {module:api/VideoApi~detailsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Object}
     */
    this.details = function(apiKey, videoId, callback) {
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling details";
      }

      // verify the required parameter 'videoId' is set
      if (videoId == undefined || videoId == null) {
        throw "Missing the required parameter 'videoId' when calling details";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'video_id': videoId
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = Object;

      return this.apiClient.callApi(
        '/video/details', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the query operation.
     * @callback module:api/VideoApi~queryCallback
     * @param {String} error Error message, if any.
     * @param {Array.<Object>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Perform a JavaScript query to return video objects matching any desired criteria.
     * Find videos matching any criteria, by running a JavaScript function over each video object. A detailed tutorial on how to use this functionality is available on the documentation page.
     * @param {String} apiKey 
     * @param {String} filter JavaScript code to be run over each video object, to determine what should be returend.
     * @param {module:api/VideoApi~queryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Array.<Object>}
     */
    this.query = function(apiKey, filter, callback) {
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling query";
      }

      // verify the required parameter 'filter' is set
      if (filter == undefined || filter == null) {
        throw "Missing the required parameter 'filter' when calling query";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'filter': filter
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = [Object];

      return this.apiClient.callApi(
        '/video/query', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the update operation.
     * @callback module:api/VideoApi~updateCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update a video&#39;s metadata.
     * Update a video&#39;s metadata through JavaScript code. Only fields inside the \&quot;userdata\&quot; object can be set.
     * @param {String} apiKey 
     * @param {String} videoId The ID of the video whose metadata will be updated
     * @param {String} source JavaScript code to execute on the video object.
     * @param {module:api/VideoApi~updateCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {Object}
     */
    this.update = function(apiKey, videoId, source, callback) {
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling update";
      }

      // verify the required parameter 'videoId' is set
      if (videoId == undefined || videoId == null) {
        throw "Missing the required parameter 'videoId' when calling update";
      }

      // verify the required parameter 'source' is set
      if (source == undefined || source == null) {
        throw "Missing the required parameter 'source' when calling update";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'video_id': videoId,
        'source': source
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = Object;

      return this.apiClient.callApi(
        '/video/update', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the upload operation.
     * @callback module:api/VideoApi~uploadCallback
     * @param {String} error Error message, if any.
     * @param {module:model/UploadParameterObject} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Return parameters needed for uploading a video file.
     * Return parameters needed for uploading a video file to Amazon Simple Storage Service. See http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-post-example.html as well as the language-specific code-examples.\n#### *Example request*\n```shell\ncurl -s https://api.synq.fm/v1/video/upload \\\n  -F api_key=${SYNQ_API_KEY} \\\n  -F video_id=2d81c30ce62f4dfdb501dbca96c7ae56\n```\n\n#### *Example response*\n```json\n{\n  \&quot;action\&quot;: \&quot;https://synqfm.s3.amazonaws.com/\&quot;,\n  \&quot;AWSAccessKeyId\&quot;: \&quot;AKIAIP77Y7MMX3ITZMFA\&quot;,\n  \&quot;Content-Type\&quot;: \&quot;video/mp4\&quot;,\n  \&quot;Policy\&quot;: \&quot;eyJleHBpcmF0aW9uIiA6ICIyMDE2LTA0LTIyVDE5OjAyOjI2LjE3MloiLCAiY29uZGl0aW9ucyIgOiBbeyJidWNrZXQiIDogInN5bnFmbSJ9LCB7ImFjbCIgOiAicHVibGljLXJlYWQifSwgWyJzdGFydHMtd2l0aCIsICIka2V5IiwgInByb2plY3RzLzZlLzYzLzZlNjNiNzUyYTE4NTRkZGU4ODViNWNjNDcyZWRmNTY5L3VwbG9hZHMvdmlkZW9zLzJkLzgxLzJkODFjMzBjZTYyZjRkZmRiNTAxZGJjYTk2YzdhZTU2Lm1wNCJdLCBbInN0YXJ0cy13aXRoIiwgIiRDb250ZW50LVR5cGUiLCAidmlkZW8vbXA0Il0sIFsiY29udGVudC1sZW5ndGgtcmFuZ2UiLCAwLCAxMDk5NTExNjI3Nzc2XV19\&quot;,\n  \&quot;Signature\&quot;: \&quot;ysqDemlKXKr6hKzVFP0hCGgf/cs=\&quot;,\n  \&quot;acl\&quot;: \&quot;public-read\&quot;,\n  \&quot;key\&quot;: \&quot;projects/6e/63/6e63b752a1854dde885b5cc472edf569/uploads/videos/2d/81/2d81c30ce62f4dfdb501dbca96c7ae56.mp4\&quot;\n}\n```\n\nTo upload the file, you can then make a multipart POST request to the URL in `action`, and for all the other parameters returned, set them as form parameters.\n\nGiven the parameters above, you would upload a file `test.mp4` using cURL like this:\n\n```shell\ncurl -s https://synqfm.s3.amazonaws.com/ \\\n  -F AWSAccessKeyId=\&quot;AKIAIP77Y7MMX3ITZMFA\&quot; \\\n  -F Content-Type=\&quot;video/mp4\&quot; \\\n  -F Policy=\&quot;eyJleHBpcmF0aW9uIiA6ICIyMDE2LTA0LTIyVDE5OjAyOjI2LjE3MloiLCAiY29uZGl0aW9ucyIgOiBbeyJidWNrZXQiIDogInN5bnFmbSJ9LCB7ImFjbCIgOiAicHVibGljLXJlYWQifSwgWyJzdGFydHMtd2l0aCIsICIka2V5IiwgInByb2plY3RzLzZlLzYzLzZlNjNiNzUyYTE4NTRkZGU4ODViNWNjNDcyZWRmNTY5L3VwbG9hZHMvdmlkZW9zLzJkLzgxLzJkODFjMzBjZTYyZjRkZmRiNTAxZGJjYTk2YzdhZTU2Lm1wNCJdLCBbInN0YXJ0cy13aXRoIiwgIiRDb250ZW50LVR5cGUiLCAidmlkZW8vbXA0Il0sIFsiY29udGVudC1sZW5ndGgtcmFuZ2UiLCAwLCAxMDk5NTExNjI3Nzc2XV19\&quot; \\\n  -F Signature=\&quot;ysqDemlKXKr6hKzVFP0hCGgf/cs=\&quot; \\\n  -F acl=\&quot;public-read\&quot; \\\n  -F key=\&quot;projects/6e/63/6e63b752a1854dde885b5cc472edf569/uploads/videos/2d/81/2d81c30ce62f4dfdb501dbca96c7ae56.mp4\&quot; \\\n  -F file=\&quot;@my_video_file.mp4\&quot;\n```\n\n
     * @param {String} apiKey 
     * @param {String} videoId The ID of the video you are going to upload into. The video needs to have been previously created.
     * @param {module:api/VideoApi~uploadCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/UploadParameterObject}
     */
    this.upload = function(apiKey, videoId, callback) {
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling upload";
      }

      // verify the required parameter 'videoId' is set
      if (videoId == undefined || videoId == null) {
        throw "Missing the required parameter 'videoId' when calling upload";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'video_id': videoId
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['application/json'];
      var returnType = UploadParameterObject;

      return this.apiClient.callApi(
        '/video/upload', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the uploader operation.
     * @callback module:api/VideoApi~uploaderCallback
     * @param {String} error Error message, if any.
     * @param {module:model/InlineResponse200} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Return embeddable url to an uploader widget
     * Returns an embeddable url, that contains an uploader widget that allows you to easily upload any mp4. Great way to simplify the uploading process for end users.
     * @param {String} apiKey 
     * @param {String} videoId The ID of the video you are going to upload into. The video needs to have been previously created.
     * @param {Object} opts Optional parameters
     * @param {String} opts.timeout How long the uploader widget works for. Anything from &#39;30 minutes&#39; to &#39;2 days&#39;. (default to 2 hours)
     * @param {module:api/VideoApi~uploaderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {module:model/InlineResponse200}
     */
    this.uploader = function(apiKey, videoId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'apiKey' is set
      if (apiKey == undefined || apiKey == null) {
        throw "Missing the required parameter 'apiKey' when calling uploader";
      }

      // verify the required parameter 'videoId' is set
      if (videoId == undefined || videoId == null) {
        throw "Missing the required parameter 'videoId' when calling uploader";
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'api_key': apiKey,
        'video_id': videoId,
        'timeout': opts['timeout']
      };

      var authNames = [];
      var contentTypes = ['multipart/form-data', 'application/x-www-form-urlencoded'];
      var accepts = ['text/plain'];
      var returnType = InlineResponse200;

      return this.apiClient.callApi(
        '/video/uploader', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
