(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Synq) {
      root.Synq = {};
    }
    root.Synq.UploadParameterObject = factory(root.Synq.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The UploadParameterObject model module.
   * @module model/UploadParameterObject
   * @version 1.7.0
   */

  /**
   * Constructs a new <code>UploadParameterObject</code>.
   * @alias module:model/UploadParameterObject
   * @class
   */
  var exports = function() {








  };

  /**
   * Constructs a <code>UploadParameterObject</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UploadParameterObject} obj Optional instance to populate.
   * @return {module:model/UploadParameterObject} The populated <code>UploadParameterObject</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('AWSAccessKeyId')) {
        obj['AWSAccessKeyId'] = ApiClient.convertToType(data['AWSAccessKeyId'], 'String');
      }
      if (data.hasOwnProperty('Content-Type')) {
        obj['Content-Type'] = ApiClient.convertToType(data['Content-Type'], 'String');
      }
      if (data.hasOwnProperty('Policy')) {
        obj['Policy'] = ApiClient.convertToType(data['Policy'], 'String');
      }
      if (data.hasOwnProperty('Signature')) {
        obj['Signature'] = ApiClient.convertToType(data['Signature'], 'String');
      }
      if (data.hasOwnProperty('acl')) {
        obj['acl'] = ApiClient.convertToType(data['acl'], 'String');
      }
      if (data.hasOwnProperty('key')) {
        obj['key'] = ApiClient.convertToType(data['key'], 'String');
      }
      if (data.hasOwnProperty('action')) {
        obj['action'] = ApiClient.convertToType(data['action'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {String} AWSAccessKeyId
   */
  exports.prototype['AWSAccessKeyId'] = undefined;

  /**
   * @member {String} Content-Type
   */
  exports.prototype['Content-Type'] = undefined;

  /**
   * @member {String} Policy
   */
  exports.prototype['Policy'] = undefined;

  /**
   * @member {String} Signature
   */
  exports.prototype['Signature'] = undefined;

  /**
   * @member {String} acl
   */
  exports.prototype['acl'] = undefined;

  /**
   * @member {String} key
   */
  exports.prototype['key'] = undefined;

  /**
   * @member {String} action
   */
  exports.prototype['action'] = undefined;




  return exports;
}));
