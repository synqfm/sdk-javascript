const expect = require('expect.js');
const synq = require('../src/index.js');
const assert = require('assert');
const request = require('request');
const fs = require('fs');

var SYNQ_API_KEY = process.env.SYNQ_API_KEY;
assert(typeof(SYNQ_API_KEY) == 'string', "The environment variable 'SYNQ_API_KEY' needs to be set before running this test");
// assert(SYNQ_API_KEY.length == 32, "Your environment variable 'SYNQ_API_KEY' holds an invalid value");

describe('video', function(){
	var video = new synq.VideoApi();

	it('is an object', function(){
		expect(video).to.be.an('object');
	});

	describe('create', function(){

		it('is a function', function() {
		    expect(video.create).to.be.a('function');
		});

		it('returns status 200', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				expect(response.statusCode).to.equal(200);
				done();
			});
		});
		it('returns a video object', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				expect(data).to.be.an('object');
				expect(data.video_id).to.be.a('string');
				expect(data.video_id.length).to.equal(32);
				done();
			});
		});
		it('returns a video object with custom metadata', function(done){
			var strkey = makeid();
			var strval = makeid();
			video.create(SYNQ_API_KEY, {userdata:'{"' + strkey + '":"' + strval + '"}'}, function(error, data, response){
				expect(data).to.be.an('object');
				expect(data.userdata).to.be.an('object');
				expect(data.userdata[strkey]).to.be.a('string');
				expect(data.userdata[strkey]).to.equal(strval);
				done();
			});
		});
	});

	describe('update', function(){
		var update = video.update;
		it('is a function', function(){
			expect(update).to.be.a('function');
		});

		it('returns status 200', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.update(SYNQ_API_KEY,data.video_id,'return video',function(error, data, response){
					expect(response.statusCode).to.equal(200);
					done();
				});
			});
		});

		it('returns video metadata',function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.update(SYNQ_API_KEY,data.video_id,'return video',function(error, data, response){
					expect(data).to.be.an('object');
					expect(data.video_id).to.be.a('string');
					expect(data.video_id.length).to.equal(32);
					done();
				});
			});
		});
		it('can update video userdata', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				var strkey = makeid();
				var strval = makeid();
				video.update(SYNQ_API_KEY,data.video_id,'video.userdata["' + strkey +'"] = "' + strval + '"; return video',function(error, data, response){
					expect(data).to.be.an('object');
					expect(data.userdata).to.be.an('object');
					expect(data.userdata[strkey]).to.equal(strval);
					done();
				});
			});
		});
	})

	describe('details', function(){
		var details = video.details;

		it('is a function', function() {
		    expect(details).to.be.a('function');
		});

		it('returns status 200', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.details(SYNQ_API_KEY,data.video_id,function(error, data, response){
					expect(response.statusCode).to.equal(200);
					done();
				});
			});
		});

		it('returns video metadata',function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.details(SYNQ_API_KEY,data.video_id,function(error, data, response){
					expect(data).to.be.an('object');
					expect(data.video_id).to.be.a('string');
					expect(data.video_id.length).to.equal(32);
					done();
				});
			});
		});
		// it('returns only the desired metadata',function(done){
		// 	video.create(SYNQ_API_KEY, {}, function(error, data, response){
		// 		video.details(SYNQ_API_KEY,data.video_id,{keys: '["video_id","created_at"]'},function(error, data, response){
		// 			expect(data).to.be.an('object');
		// 			expect(Object.keys(data).length).to.equal(2);
		// 			expect(data.video_id).to.be.a('string');
		// 			expect(data.video_id.length).to.equal(32);
		// 			expect(data.created_at).to.be.a('string');
		// 			done();
		// 		});
		// 	});
		//  });
	});

	// describe('queryExact', function(){

	// 	it('is a function', function() {
	// 	    expect(video.queryExact).to.be.a('function');
	// 	});

	// 	it('returns status 200', function(done){
	// 		video.queryExact(SYNQ_API_KEY, '{}', {},function(error, data, response){
	// 			expect(response.statusCode).to.equal(200);
	// 			done();
	// 		});
	// 	});

	// 	it('returns an array of video objects', function(done){
	// 		video.queryExact(SYNQ_API_KEY, '{}',{}, function(error, data, response){
	// 			expect(data).to.be.an('array');
	// 			expect(data[0]).to.be.an('object');
	// 			expect(data[0].video_id).to.be.a('string');
	// 			expect(data[0].video_id.length).to.equal(32);
	// 			done();
	// 		});
	// 	});
	// 	it('returns only the desired video objects', function(done){
	// 		var strkey = makeid();
	// 		var strval = makeid();
	// 		video.create(SYNQ_API_KEY, {metadata: '{"userdata":{"' + strkey + '":"' + strval + '"}}'}, function(error, data, response){
	// 			video.queryExact(SYNQ_API_KEY, '{"userdata":{"' + strkey + '":"' + strval + '"}}',{}, function(error, data, response){
	// 				expect(data).to.be.an('array');
	// 				expect(data.length).to.equal(1);
	// 				expect(data[0]).to.be.an('object');
	// 				expect(data[0].userdata[strkey]).to.equal(strval);
	// 				done();
	// 			});
	// 		});
	// 	});
	// 	it('returns only the desired keys', function(done){
	// 		var strkey = makeid();
	// 		var strval = makeid();
	// 		video.create(SYNQ_API_KEY, {metadata: '{"userdata":{"' + strkey + '":"' + strval + '"}}'}, function(error, data, response){
	// 			expect(data.video_id).to.be.a('string');
	// 			var video_id = data.video_id;
	// 			video.queryExact(SYNQ_API_KEY, '{"userdata":{"' + strkey + '":"' + strval + '"}}',{keys:'["video_id","created_at"]'}, function(error, data, response){
	// 				expect(data).to.be.an('array');
	// 				expect(data.length).to.equal(1);
	// 				expect(data[0]).to.be.an('object');
	// 				expect(Object.keys(data[0]).length).to.equal(2);
	// 				expect(data[0].video_id).to.be.a('string');
	// 				expect(data[0].video_id.length).to.equal(32);
	// 				expect(data[0].created_at).to.be.a('string');
	// 				expect(data[0].video_id).to.equal(video_id);
	// 				done();
	// 			});
	// 		});
	// 	});
		
	// });

	describe('query', function(){

		this.timeout(5000);

		it('is a function', function() {
		    expect(video.query).to.be.a('function');
		});

		it('returns status 200', function(done){
			video.query(SYNQ_API_KEY, 'return', function(error, data, response){
				expect(response.statusCode).to.equal(200);
				done();
			});
		});

		it('returns an array of video objects', function(done){
			video.query(SYNQ_API_KEY, 'return video', function(error, data, response){
				expect(data).to.be.an('array');
				expect(data[0]).to.be.an('object');
				expect(data[0].video_id).to.be.a('string');
				expect(data[0].video_id.length).to.equal(32);
				done();
			});
		});
		it('returns only the desired video objects', function(done){
			var strkey = makeid();
			var strval = makeid();
			video.create(SYNQ_API_KEY, {userdata: '{"' + strkey + '":"' + strval + '"}'}, function(error, data, response){
				expect(data.video_id).to.be.a('string');
				var video_id = data.video_id;
				video.query(SYNQ_API_KEY, 'return video.userdata["' + strkey + '"] == "' + strval + '" ? video : undefined', function(error, data, response){
					expect(data).to.be.an('array');
					expect(data.length).to.equal(1);
					expect(data[0]).to.be.an('object');
					expect(data[0].userdata[strkey]).to.equal(strval);
					done();
				});
			});
		});
		it('returns only the desired keys', function(done){
			var strkey = makeid();
			var strval = makeid();
			video.create(SYNQ_API_KEY, {userdata: '{"' + strkey + '":"' + strval + '"}'}, function(error, data, response){
				expect(data.video_id).to.be.a('string');
				var video_id = data.video_id;
				video.query(SYNQ_API_KEY, 'return video.userdata["' + strkey + '"] == "' + strval + '" ? video.pick("video_id", "created_at") : undefined', function(error, data, response){
					expect(data).to.be.an('array');
					expect(data.length).to.equal(1);
					expect(data[0]).to.be.an('object');
					expect(Object.keys(data[0]).length).to.equal(2);
					expect(data[0].video_id).to.be.a('string');
					expect(data[0].video_id.length).to.equal(32);
					expect(data[0].created_at).to.be.a('string');
					expect(data[0].video_id).to.equal(video_id);
					done();
				});
			});
		});
		
	});

	describe('upload', function(){

		this.timeout(10000);
		it('is a function', function() {
		    expect(video.upload).to.be.a('function');
		});

		it('returns status 200', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.upload(SYNQ_API_KEY,data.video_id,function(error, data, response){
					expect(response.statusCode).to.equal(200);
					done();
				});
			});
		});
		it('returns amazon upload information',function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.upload(SYNQ_API_KEY,data.video_id,function(error, data, response){
					expect(data).to.be.an('object');
					expect(Object.keys(data).length).to.equal(7);
					expect(data.action).to.be.a('string');
					done();
				});
			});
		});
		it('returns upload information that works',function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.upload(SYNQ_API_KEY, data.video_id, function(error, data, response){
					expect(data).to.be.an('object');
					expect(Object.keys(data).length).to.equal(7);
					expect(data.action).to.be.a('string');
					
					var url = data["action"];
					var file = fs.createReadStream('test.mp4');
					var upload_data = {
						"AWSAccessKeyId" : data['AWSAccessKeyId'],
						"Content-Type": data['Content-Type'],
						"Policy": data['Policy'],
						"Signature": data['Signature'],
						"acl": data['acl'],
						"key": data['key'],
						"file": file
					};
					request.post({url: url, formData: upload_data}, function(error, response, body){
						expect(response.statusCode).to.equal(204);
						done();
					});
				});
			});
		});
		
	});

		describe('uploader', function(){

		this.timeout(10000);
		it('is a function', function() {
		    expect(video.uploader).to.be.a('function');
		});

		it('returns status 200', function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.uploader(SYNQ_API_KEY,data.video_id,{},function(error, data, response){
					expect(response.statusCode).to.equal(200);
					done();
				});
			});
		});
		it('returns a json with uploader url',function(done){
			video.create(SYNQ_API_KEY, {}, function(error, data, response){
				video.uploader(SYNQ_API_KEY,data.video_id,{},function(error, data, response){
					expect(data).to.be.an('object');
					expect(Object.keys(data).length).to.equal(1);
					expect(data.uploader_url).to.be.a('string');
					done();
				});
			});
		});
	});



	// describe('stream', function(){


	// 	it('is a function', function() {
	// 	    expect(video.stream).to.be.a('function');
	// 	});
	// 	it('returns status 200', function(done){
	// 		video.create(SYNQ_API_KEY, {}, function(error, data, response){
	// 			video.stream(SYNQ_API_KEY,data.video_id,function(error, data, response){
	// 				expect(response.statusCode).to.equal(200);
	// 				done();
	// 			});
	// 		});
	// 	});
	// 	it('returns streaming information',function(done){
	// 		video.create(SYNQ_API_KEY, {}, function(error, data, response){
	// 			video.stream(SYNQ_API_KEY,data.video_id,function(error, data, response){
	// 				expect(data).to.be.an('object');
	// 				expect(Object.keys(data).length).to.equal(4);
	// 				expect(data.rtmp_host).to.be.a('string');
	// 				done();
	// 			});
	// 		});
	// 	});
	// });
});

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}