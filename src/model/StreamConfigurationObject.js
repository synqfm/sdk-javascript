(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Synq) {
      root.Synq = {};
    }
    root.Synq.StreamConfigurationObject = factory(root.Synq.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The StreamConfigurationObject model module.
   * @module model/StreamConfigurationObject
   * @version 1.7.0
   */

  /**
   * Constructs a new <code>StreamConfigurationObject</code>.
   * @alias module:model/StreamConfigurationObject
   * @class
   */
  var exports = function() {





  };

  /**
   * Constructs a <code>StreamConfigurationObject</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/StreamConfigurationObject} obj Optional instance to populate.
   * @return {module:model/StreamConfigurationObject} The populated <code>StreamConfigurationObject</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('rtmp_host')) {
        obj['rtmp_host'] = ApiClient.convertToType(data['rtmp_host'], 'String');
      }
      if (data.hasOwnProperty('rtmp_port')) {
        obj['rtmp_port'] = ApiClient.convertToType(data['rtmp_port'], 'String');
      }
      if (data.hasOwnProperty('rtmp_key')) {
        obj['rtmp_key'] = ApiClient.convertToType(data['rtmp_key'], 'String');
      }
      if (data.hasOwnProperty('rtmp_url')) {
        obj['rtmp_url'] = ApiClient.convertToType(data['rtmp_url'], 'String');
      }
    }
    return obj;
  }


  /**
   * @member {String} rtmp_host
   */
  exports.prototype['rtmp_host'] = undefined;

  /**
   * @member {String} rtmp_port
   */
  exports.prototype['rtmp_port'] = undefined;

  /**
   * @member {String} rtmp_key
   */
  exports.prototype['rtmp_key'] = undefined;

  /**
   * @member {String} rtmp_url
   */
  exports.prototype['rtmp_url'] = undefined;




  return exports;
}));
