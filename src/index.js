(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['./ApiClient', './model/ErrorObject', './model/InlineResponse200', './model/StreamConfigurationObject', './model/UploadParameterObject', './api/VideoApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./model/ErrorObject'), require('./model/InlineResponse200'), require('./model/StreamConfigurationObject'), require('./model/UploadParameterObject'), require('./api/VideoApi'));
  }
}(function(ApiClient, ErrorObject, InlineResponse200, StreamConfigurationObject, UploadParameterObject, VideoApi) {
  'use strict';

  /**
   * The SYNQ video API.<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var Synq = require('./index'); // See note below*.
   * var xxxSvc = new Synq.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new Synq.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['./index'], function(){...}) and put the application logic within the
   * callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new Synq.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new Synq.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 1.7.0
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The ErrorObject model constructor.
     * @property {module:model/ErrorObject}
     */
    ErrorObject: ErrorObject,
    /**
     * The InlineResponse200 model constructor.
     * @property {module:model/InlineResponse200}
     */
    InlineResponse200: InlineResponse200,
    /**
     * The StreamConfigurationObject model constructor.
     * @property {module:model/StreamConfigurationObject}
     */
    StreamConfigurationObject: StreamConfigurationObject,
    /**
     * The UploadParameterObject model constructor.
     * @property {module:model/UploadParameterObject}
     */
    UploadParameterObject: UploadParameterObject,
    /**
     * The VideoApi service constructor.
     * @property {module:api/VideoApi}
     */
    VideoApi: VideoApi
  };

  return exports;
}));
