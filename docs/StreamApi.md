# Synq.StreamApi

All URIs are relative to *http://api.staging.synq.fm/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**start**](StreamApi.md#start) | **POST** /stream/start | Return parameters needed for transmitting a video stream to our RTMP receiver.


<a name="start"></a>
# **start**
> StreamConfigurationObject start(apiKey, videoId)

Return parameters needed for transmitting a video stream to our RTMP receiver.

Return parameters needed for transmitting a video stream to our RTMP receiver.\n\n#### *Example request*:\n\n```shell\ncurl -s https://api.synq.fm/v1/stream/start \\\n    -F api_key=${SYNQ_API_KEY} \\\n    -F video_id=2b795c9e64648e08100ce6df6cf4b989\n```\n\n#### *Example response*:\n\n```json\n{\n    \&quot;rtmp_host\&quot;: \&quot;ec2-54-171-205-23.eu-west-1.compute.amazonaws.com\&quot;,\n    \&quot;rtmp_port\&quot;: \&quot;1935\&quot;,\n    \&quot;rtmp_key\&quot;: \&quot;live/ce29d1cb5da48a564d2c9f62be7ade06\&quot;,\n    \&quot;rtmp_url\&quot;: \&quot;rtmp://ec2-54-171-205-23.eu-west-1.compute.amazonaws.com:1935/live/ce29d1cb5da48a564d2c9f62be7ade06\&quot;\n}\n```\n\nTo transmit the video stream, enter the parameters into your streaming software.\nFor example, for FFmpeg:\n\n```shell\n    ffmpeg -re -i my_video_file.mp4 -f flv rtmp://ec2-54-171-205-23.eu-west-1.compute.amazonaws.com:1935/live/ce29d1cb5da48a564d2c9f62be7ade06\n```\n\nAfter two hours of streaming or fifteen minutes after the last video packet was received,\nwhichever happends first, the stream is considered done.\nAt that point, the recording will be processed for later playback,\nand you will be unable to continue streaming this particular video.\n

### Example
```javascript
var Synq = require('synq');

var apiInstance = new Synq.StreamApi()

var apiKey = "apiKey_example"; // {String} 

var videoId = "videoId_example"; // {String} The ID of the video you are going to stream into.


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
api.start(apiKey, videoId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**|  | 
 **videoId** | **String**| The ID of the video you are going to stream into. | 

### Return type

[**StreamConfigurationObject**](StreamConfigurationObject.md)

### Authorization

No authorization required

### HTTP reuqest headers

 - **Content-Type**: multipart/form-data, application/x-www-form-urlencoded
 - **Accept**: application/json

