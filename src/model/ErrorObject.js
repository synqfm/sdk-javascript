(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['../ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.Synq) {
      root.Synq = {};
    }
    root.Synq.ErrorObject = factory(root.Synq.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The ErrorObject model module.
   * @module model/ErrorObject
   * @version 1.7.0
   */

  /**
   * Constructs a new <code>ErrorObject</code>.
   * @alias module:model/ErrorObject
   * @class
   * @param url
   * @param name
   * @param message
   */
  var exports = function(url, name, message) {

    this['url'] = url;
    this['name'] = name;
    this['message'] = message;

  };

  /**
   * Constructs a <code>ErrorObject</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ErrorObject} obj Optional instance to populate.
   * @return {module:model/ErrorObject} The populated <code>ErrorObject</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) { 
      obj = obj || new exports();

      if (data.hasOwnProperty('url')) {
        obj['url'] = ApiClient.convertToType(data['url'], 'String');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('message')) {
        obj['message'] = ApiClient.convertToType(data['message'], 'String');
      }
      if (data.hasOwnProperty('details')) {
        obj['details'] = ApiClient.convertToType(data['details'], Object);
      }
    }
    return obj;
  }


  /**
   * @member {String} url
   */
  exports.prototype['url'] = undefined;

  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;

  /**
   * @member {String} message
   */
  exports.prototype['message'] = undefined;

  /**
   * @member {Object} details
   */
  exports.prototype['details'] = undefined;




  return exports;
}));
